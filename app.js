const express = require('express');
const app = express();

const path = require('path');
app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
const expressLayouts = require('express-ejs-layouts');
app.use(expressLayouts);
app.set('layout', '../views/layouts/layout');


app.use(express.json());
app.use(express.urlencoded({ extended: false }));

require("./controllers/mongoose_init")

const cocktailsRouter = require("./routes/cocktailsRouter");
app.use("/cocktails", cocktailsRouter);

app.get("*", (req, res) => {
    res.redirect('/cocktails')
})

module.exports = app;