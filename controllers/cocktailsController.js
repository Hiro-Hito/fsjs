const Cocktail = require("../models/Cocktails");

const PUBLIC_DIR = "./public" ;
const UPLOAD_DIR = '/data/uploads/' ;
const multer  = require('multer') ;
const fs = require('fs') ;


// --- ROUTES ---

module.exports.list = (req, res, next)=>{
    Cocktail.find()
        .then(cocktails => {
            res.render('pages/cocktailsList', { cocktails }) ;
        })
        .catch(error => res.status(400).send(error))
}

module.exports.select = (req, res, next)=> {
    const id = req.params.id;
    Cocktail.findById(id)
        .then((cocktail) => {
            res.render('./pages/cocktailInfos', {cocktail})
        })
        .catch(error => res.status(400).send(error))

}

module.exports.filter = (req, res, next)=> {
    const letter = req.params.letter ;
    Cocktail.find({ 'name': { $regex: '^(?i)' + letter } })
        .then(cocktails => {
            res.render('pages/cocktailsList', { cocktails }) ;
        })
        .catch(error => res.status(400).send(error))
}
