// Importation du module mongoose
const mongoose = require('mongoose');

// Connection mongoose à MongoDB
const host = process.env.MONGO_HOST ;
const db_name = process.env.MONGO_DB_NAME ;
const db_port = process.env.MONGO_PORT ;

const user = process.env.MONGO_USER ;
const pwd = process.env.MONGO_PWD ;

const mongoDB = `mongodb://${host}:${db_port}/${db_name}?retryWrites=true&w=majority` ;
mongoose.connect(mongoDB, { useUnifiedTopology: true })
    .then(() => console.log('MongoDB OK !'))
    .catch(() => console.log('MongoDB ERREUR !'));

// Get the default connection
const db = mongoose.connection;

// Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


