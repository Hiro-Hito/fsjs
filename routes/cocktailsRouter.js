const express = require('express');
const router = express.Router();

const cocktailsController = require('../controllers/cocktailsController');

router.get('/', cocktailsController.list);

router.get('/:id', cocktailsController.select)

router.get('/filter/:letter', cocktailsController.filter);

module.exports = router; 
 