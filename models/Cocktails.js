const mongoose = require('mongoose') ;

const cocktailSchema = mongoose.Schema({

    name: { type: String, required: true },
    instructions: { type: String, default: "Information manquante" },
    image: { type: String, required: true},
    ingredients: { type: String, default: "Information manquante" }
}) ;


module.exports = mongoose.model('cocktails_db', cocktailSchema, 'cocktails') ;